<?php 
$nilai = "72 65 73 78 75 74 90 81 87 65 55 69 72 78 79 91 100 40 67 77 86";
$arr_nilai = explode(" ", $nilai);

$arr_length = count($arr_nilai);
$arr_sum = array_sum($arr_nilai);

// Nilai rata-rata
$rata_rata = $arr_sum / $arr_length;
echo "Nilai rata-ratanya adalah: " . $rata_rata;
echo '<br />';
echo '<br />';

// 7 Nilai terendah
sort($arr_nilai);
$arr = array_slice($arr_nilai, 0, 7);
$arr_length = count($arr);

echo "7 Nilai terendah adalah: ";
for ($i=0; $i < $arr_length; $i++) { 
  echo $arr[$i].' ';
}

echo '<br />';
echo '<br />';

// 7 Nilai tertinggi
$arr = array_slice($arr_nilai, -7, 7);
$arr_length = count($arr);
echo "7 Nilai tertinggi adalah: ";

for ($i=0; $i < $arr_length; $i++) { 
  echo $arr[$i].' ';
}

?>