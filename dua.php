<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Soal PHP 2</title>
</head>
<body>
  <h3>Hitung Jumlah Huruf Kecil</h3>
  <form action="dua.php" method="post">
    <label for="fname">Masukkan huruf:</label>
    <input type="text" name="text">
    <input type="submit" name="submit" value="Hitung" />
  </form>
</body>
</html>

<?php 
  function calculateLowerCase($text) {
    $upperCase = strtoupper($text);
    $upperDiff = similar_text($text, $upperCase);
    $count_lower = strlen($text) - $upperDiff;
    
    echo '"' . $text . '"' . ' mengandung ' . $count_lower . ' buah huruf kecil.';
  }

  if(isset($_POST['submit'])) {
    $text = $_POST['text']; 

    calculateLowerCase($text);
  }
?>