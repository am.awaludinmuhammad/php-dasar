<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Soal PHP 2</title>
</head>
<body>
  <h3>Encrypt</h3>
  <form action="encrypt.php" method="post">
    <label for="fname">Masukkan text:</label>
    <input type="text" name="text">
    <input type="submit" name="submit" value="Hitung" />
  </form>
</body>
</html>

<?php
	//  D  F  H  K  N  Q
  //  E  D  K  G  S  K
	// +1 -2 +3 -4 +5 -6

	function encrypt($text){
		$alphabet = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
	  
		$text = strtoupper($text);
		$arr_text = str_split($text);

    // nilai awal
		$plus = true;
		$result = '';
		$x = 1; 
		$text_index = 0;

    $arr_length = count($arr_text); //jumlah huruf yang diinput
		for ($i=0; $i < $arr_length; $i++) { 
			$text_index = array_search($arr_text[$i], $alphabet); // cari input adalah index ke berapa dari alphabet.
			if ($plus == true) { //pola plus
				$result .= $alphabet[$text_index + $x];
				$plus = false;
			} else { //pola minus
				$y = $text_index - $x;
				if ($y < 0) {
					$y = count($alphabet) + ($y);
				}
        $result .= $alphabet[$y];
				$plus = true;
			}
			$x++;
		}
		
	  return $result;
	}

  if (isset($_POST['submit'])) {
    echo encrypt($_POST['text']);
  }
?>